From iris_ora.algebra Require Export ora.
From iris_ora.logic Require Export oupred.
Set Default Proof Using "Type".
Local Arguments pcore _ _ !_ /.
Local Arguments ora_pcore _ !_ /.
Local Arguments validN _ _ _ !_ /.
Local Arguments valid _ _  !_ /.
Local Arguments ora_validN _ _ !_ /.
Local Arguments ora_valid _  !_ /.

Inductive osum (A B : Type) :=
  | Oinl : A → osum A B
  | Oinr : B → osum A B
  | OsumBot : osum A B.
Arguments Oinl {_ _} _.
Arguments Oinr {_ _} _.
Arguments OsumBot {_ _}.

Instance: Params (@Oinl) 2 := {}.
Instance: Params (@Oinr) 2 := {}.
Instance: Params (@OsumBot) 2 := {}.

Instance maybe_Oinl {A B} : Maybe (@Oinl A B) := λ x,
  match x with Oinl a => Some a | _ => None end.
Instance maybe_Oinr {A B} : Maybe (@Oinr A B) := λ x,
  match x with Oinr b => Some b | _ => None end.

Section cofe.
Context {A B : ofe}.
Implicit Types a : A.
Implicit Types b : B.

(* Cofe *)
Inductive osum_equiv : Equiv (osum A B) :=
  | Oinl_equiv a a' : a ≡ a' → Oinl a ≡ Oinl a'
  | Oinr_equiv b b' : b ≡ b' → Oinr b ≡ Oinr b'
  | OsumBot_equiv : OsumBot ≡ OsumBot.
Existing Instance osum_equiv.
Inductive osum_dist : Dist (osum A B) :=
  | Oinl_dist n a a' : a ≡{n}≡ a' → Oinl a ≡{n}≡ Oinl a'
  | Oinr_dist n b b' : b ≡{n}≡ b' → Oinr b ≡{n}≡ Oinr b'
  | OsumBot_dist n : OsumBot ≡{n}≡ OsumBot.
Existing Instance osum_dist.

Global Instance Oinl_ne : NonExpansive (@Oinl A B).
Proof. by constructor. Qed.
Global Instance Oinl_proper : Proper ((≡) ==> (≡)) (@Oinl A B).
Proof. by constructor. Qed.
Global Instance Oinl_inj : Inj (≡) (≡) (@Oinl A B).
Proof. by inversion_clear 1. Qed.
Global Instance Oinl_inj_dist n : Inj (dist n) (dist n) (@Oinl A B).
Proof. by inversion_clear 1. Qed.
Global Instance Oinr_ne : NonExpansive (@Oinr A B).
Proof. by constructor. Qed.
Global Instance Oinr_proper : Proper ((≡) ==> (≡)) (@Oinr A B).
Proof. by constructor. Qed.
Global Instance Oinr_inj : Inj (≡) (≡) (@Oinr A B).
Proof. by inversion_clear 1. Qed.
Global Instance Oinr_inj_dist n : Inj (dist n) (dist n) (@Oinr A B).
Proof. by inversion_clear 1. Qed.

Definition osum_ofe_mixin : OfeMixin (osum A B).
Proof.
  split.
  - intros mx my; split.
    + by destruct 1; constructor; try apply equiv_dist.
    + intros Hxy; feed inversion (Hxy 0); subst; constructor; try done;
      apply equiv_dist=> n; by feed inversion (Hxy n).
  - intros n; split.
    + by intros [|a|]; constructor.
    + by destruct 1; constructor.
    + destruct 1; inversion_clear 1; constructor; etrans; eauto.
  - by inversion_clear 1; constructor; apply dist_S.
Qed.
Canonical Structure osumO : ofe := Ofe (osum A B) osum_ofe_mixin.

Program Definition osum_chain_l (c : chain osumO) (a : A) : chain A :=
  {| chain_car n := match c n return _ with Oinl a' => a' | _ => a end |}.
Next Obligation. intros c a n i ?; simpl. by destruct (chain_cauchy c n i). Qed.
Program Definition osum_chain_r (c : chain osumO) (b : B) : chain B :=
  {| chain_car n := match c n return _ with Oinr b' => b' | _ => b end |}.
Next Obligation. intros c b n i ?; simpl. by destruct (chain_cauchy c n i). Qed.
Definition osum_compl `{Cofe A, Cofe B} : Compl osumO := λ c,
  match c 0 with
  | Oinl a => Oinl (compl (osum_chain_l c a))
  | Oinr b => Oinr (compl (osum_chain_r c b))
  | OsumBot => OsumBot
  end.
Global Program Instance osum_cofe `{Cofe A, Cofe B} : Cofe osumO :=
  {| compl := osum_compl |}.
Next Obligation.
  intros ?? n c; rewrite /compl /osum_compl.
  feed inversion (chain_cauchy c 0 n); first auto with lia; constructor.
  + rewrite (conv_compl n (osum_chain_l c a')) /=. destruct (c n); naive_solver.
  + rewrite (conv_compl n (osum_chain_r c b')) /=. destruct (c n); naive_solver.
Qed.

Global Instance osum_ofe_discrete :
  OfeDiscrete A → OfeDiscrete B → OfeDiscrete osumO.
Proof. by inversion_clear 3; constructor; apply (discrete _). Qed.
Global Instance osum_leibniz :
  LeibnizEquiv A → LeibnizEquiv B → LeibnizEquiv osumO.
Proof. by destruct 3; f_equal; apply leibniz_equiv. Qed.

Global Instance Oinl_discrete a : Discrete a → Discrete (Oinl a).
Proof. by inversion_clear 2; constructor; apply (discrete _). Qed.
Global Instance Oinr_discrete b : Discrete b → Discrete (Oinr b).
Proof. by inversion_clear 2; constructor; apply (discrete _). Qed.
End cofe.

Arguments osumO : clear implicits.

(* Functor on COFEs *)
Definition osum_map {A A' B B'} (fA : A → A') (fB : B → B')
                    (x : osum A B) : osum A' B' :=
  match x with
  | Oinl a => Oinl (fA a)
  | Oinr b => Oinr (fB b)
  | OsumBot => OsumBot
  end.
Instance: Params (@osum_map) 4 := {}.

Lemma osum_map_id {A B} (x : osum A B) : osum_map id id x = x.
Proof. by destruct x. Qed.
Lemma osum_map_compose {A A' A'' B B' B''} (f : A → A') (f' : A' → A'')
                       (g : B → B') (g' : B' → B'') (x : osum A B) :
  osum_map (f' ∘ f) (g' ∘ g) x = osum_map f' g' (osum_map f g x).
Proof. by destruct x. Qed.
Lemma osum_map_ext {A A' B B' : ofe} (f f' : A → A') (g g' : B → B') x :
  (∀ x, f x ≡ f' x) → (∀ x, g x ≡ g' x) → osum_map f g x ≡ osum_map f' g' x.
Proof. by destruct x; constructor. Qed.
Instance osum_map_ora_ne {A A' B B' : ofe} n :
  Proper ((dist n ==> dist n) ==> (dist n ==> dist n) ==> dist n ==> dist n)
         (@osum_map A A' B B').
Proof. intros f f' Hf g g' Hg []; destruct 1; constructor; by apply Hf || apply Hg. Qed.
Definition osumO_map {A A' B B'} (f : A -n> A') (g : B -n> B') :
  osumO A B -n> osumO A' B' :=
  OfeMor (osum_map f g).
Instance osumO_map_ne A A' B B' :
  NonExpansive2 (@osumO_map A A' B B').
Proof. by intros n f f' Hf g g' Hg []; constructor. Qed.

Section ora.
Context {A B : oraT}.
Implicit Types a : A.
Implicit Types b : B.

(* ORA *)
Instance osum_valid : Valid (osum A B) := λ x,
  match x with
  | Oinl a => ✓ a
  | Oinr b => ✓ b
  | OsumBot => False
  end.
Instance osum_validN : ValidN (osum A B) := λ n x,
  match x with
  | Oinl a => ✓{n} a
  | Oinr b => ✓{n} b
  | OsumBot => False
  end.
Instance osum_pcore : PCore (osum A B) := λ x,
  match x with
  | Oinl a => Oinl <$> pcore a
  | Oinr b => Oinr <$> pcore b
  | OsumBot => Some OsumBot
  end.
Instance osum_op : Op (osum A B) := λ x y,
  match x, y with
  | Oinl a, Oinl a' => Oinl (a ⋅ a')
  | Oinr b, Oinr b' => Oinr (b ⋅ b')
  | _, _ => OsumBot
  end.

Instance osum_order : OraOrder (osum A B) := λ x y,
  match x, y with
  | Oinl a, Oinl a' => a ≼ₒ a'
  | Oinr b, Oinr b' => b ≼ₒ b'
  | _, OsumBot => True
  | _, _ => False
  end.

Instance osum_orderN : OraOrderN (osum A B) := λ n x y,
  match x, y with
  | Oinl a, Oinl a' => a ≼ₒ{n} a'
  | Oinr b, Oinr b' => b ≼ₒ{n} b'
  | _, OsumBot => True
  | _, _ => False
  end.

Lemma Oinl_op a a' : Oinl a ⋅ Oinl a' = Oinl (a ⋅ a').
Proof. done. Qed.
Lemma Oinr_op b b' : Oinr b ⋅ Oinr b' = Oinr (b ⋅ b').
Proof. done. Qed.

Lemma osum_included x y :
  x ≼ y ↔ y = OsumBot ∨ (∃ a a', x = Oinl a ∧ y = Oinl a' ∧ a ≼ a')
                      ∨ (∃ b b', x = Oinr b ∧ y = Oinr b' ∧ b ≼ b').
Proof.
  split.
  - unfold included. intros [[a'|b'|] Hy]; destruct x as [a|b|];
      inversion_clear Hy; eauto 10.
  - intros [->|[(a&a'&->&->&c&?)|(b&b'&->&->&c&?)]].
    + destruct x; exists OsumBot; constructor.
    + exists (Oinl c); by constructor.
    + exists (Oinr c); by constructor.
Qed.

Lemma osum_includedN n x y :
  x ≼{n} y ↔ y = OsumBot ∨ (∃ a a', x = Oinl a ∧ y = Oinl a' ∧ a ≼{n} a')
                         ∨ (∃ b b', x = Oinr b ∧ y = Oinr b' ∧ b ≼{n} b').
Proof.
  split.
  - unfold includedN. intros [[a'|b'|] Hy]; destruct x as [a|b|];
      inversion_clear Hy; eauto 10.
  - intros [->|[(a&a'&->&->&c&?)|(b&b'&->&->&c&?)]].
    + destruct x; exists OsumBot; constructor.
    + exists (Oinl c); by constructor.
    + exists (Oinr c); by constructor.
Qed.

Lemma osum_order' x y :
  x ≼ₒ y ↔ y = OsumBot ∨ (∃ a a', x = Oinl a ∧ y = Oinl a' ∧ a ≼ₒ a')
                       ∨ (∃ b b', x = Oinr b ∧ y = Oinr b' ∧ b ≼ₒ b').
Proof.
  destruct x; destruct y; split; rewrite /Oraorder /=; intuition;
  repeat match goal with
    | H : ∃ x, _ |- _ => destruct H
    | H : _ ∧ _ |- _ => destruct H
  end; simplify_eq; eauto; try (right; left; eauto; fail);
    try (right; right; eauto; fail).
Qed.

Lemma osum_orderN' n x y :
  x ≼ₒ{n} y ↔ y = OsumBot ∨ (∃ a a', x = Oinl a ∧ y = Oinl a' ∧ a ≼ₒ{n} a')
                          ∨ (∃ b b', x = Oinr b ∧ y = Oinr b' ∧ b ≼ₒ{n} b').
Proof.
  destruct x; destruct y; split; rewrite /OraorderN /=; intuition;
  repeat match goal with
    | H : ∃ x, _ |- _ => destruct H
    | H : _ ∧ _ |- _ => destruct H
  end; simplify_eq; eauto; try (right; left; eauto; fail);
    try (right; right; eauto; fail).
Qed.

Lemma Increasing_OsumBot : Increasing OsumBot.
Proof. intros [?|?|]; done. Qed.

Lemma Increasing_oinl a : Increasing a ↔ Increasing (Oinl a).
Proof.
  split.
  - intros Ha [zl|zr|]; try done; apply Ha.
  - intros Ha z; apply (Ha (Oinl z)).
Qed.

Lemma Increasing_oinr b : Increasing b ↔ Increasing (Oinr b).
Proof.
  split.
  - intros Hb [zl|zr|]; try done; apply Hb.
  - intros Hb z; apply (Hb (Oinr z)).
Qed.

Lemma Oinl_orderN n x y : x ≼ₒ{n} y ↔ (Oinl x) ≼ₒ{n} (Oinl y).
Proof. done. Qed.

Lemma Oinr_orderN n x y : x ≼ₒ{n} y ↔ (Oinr x) ≼ₒ{n} (Oinr y).
Proof. done. Qed.

Lemma Oinl_order x y : x ≼ₒ y ↔ (Oinl x) ≼ₒ (Oinl y).
Proof. done. Qed.

Lemma Oinr_order x y : x ≼ₒ y ↔ (Oinr x) ≼ₒ (Oinr y).
Proof. done. Qed.

Lemma osum_ora_mixin : OraMixin (osum A B).
Proof.
  split.
  - intros [] n; destruct 1; constructor; by ofe_subst.
  - intros ???? [n a a' Ha|n b b' Hb|n] [=]; subst; eauto.
    + destruct (pcore a) as [ca|] eqn:?; simplify_option_eq.
      destruct (ora_pcore_ne n a a' ca) as (ca'&->&?); auto.
      exists (Oinl ca'); by repeat constructor.
    + destruct (pcore b) as [cb|] eqn:?; simplify_option_eq.
      destruct (ora_pcore_ne n b b' cb) as (cb'&->&?); auto.
      exists (Oinr cb'); by repeat constructor.
  - intros ? [a|b|] [a'|b'|] H; inversion_clear H; ofe_subst; done.
  - intros [xl|xr|] [cxl|cxr|] Hcx [zl|zr|]; simpl in *;
      try destruct (pcore xl) eqn:Heq; try destruct (pcore xr) eqn:Heq;
        simpl in *; simplify_eq; try eapply ora_pcore_increasing; eauto.
  - intros n [xl|xr|] [yl|yr|]; intros Hi Ho; try inversion Ho;
      try (intros [zl|zr|]; done); rewrite /Oraorder /=;
      try apply Increasing_oinl in Hi; try apply Increasing_oinr in Hi;
    try apply Increasing_oinl; try apply Increasing_oinr;
      eauto using ora_increasing_closed.
  - intros [a|b|]; rewrite /= ?ora_valid_validN; naive_solver eauto using O.
  - intros n [a|b|]; simpl; auto using ora_validN_S.
  - intros [a1|b1|] [a2|b2|] [a3|b3|]; constructor; by rewrite ?assoc.
  - intros [a1|b1|] [a2|b2|]; constructor; by rewrite 1?comm.
  - intros [a|b|] ? [=]; subst; auto.
    + destruct (pcore a) as [ca|] eqn:?; simplify_option_eq.
      constructor; eauto using ora_pcore_l.
    + destruct (pcore b) as [cb|] eqn:?; simplify_option_eq.
      constructor; eauto using ora_pcore_l.
  - intros [a|b|] ? [=]; subst; auto.
    + destruct (pcore a) as [ca|] eqn:?; simplify_option_eq.
      feed inversion (ora_pcore_idemp a ca); repeat constructor; auto.
    + destruct (pcore b) as [cb|] eqn:?; simplify_option_eq.
      feed inversion (ora_pcore_idemp b cb); repeat constructor; auto.
  - intros n [xl|xr|] [yl|yr|] [cxl|cxr|] Ho Hcx; try done;
      simpl in *; try destruct (pcore xl) eqn:Heq;
        try destruct (pcore xr) eqn:Heq; simpl in *; simplify_eq; eauto.
    + destruct (ora_pcore_monoN n xl yl cxl) as [z [Heqy ?]]; auto.
      exists (Oinl z); by rewrite Heqy.
    + destruct (ora_pcore_monoN n xr yr cxr) as [z [Heqy ?]]; auto.
      exists (Oinr z); by rewrite Heqy.
  - intros n [xl|xr|] [yl|yr|] Hv; try inversion Hv;
      eapply ora_validN_op_l; apply Hv.
  - intros n [xl|xr|] [y1l|y1r|] [y2l|y2r|] Hx Hyx; try done.
    + destruct (ora_op_extend n xl y1l y2l) as (z1 & z2 & Hz1 & Hz2 & Hz3); auto.
      exists (Oinl z1), (Oinl z2); repeat split; by try constructor.
    + destruct (ora_op_extend n xr y1r y2r) as (z1 & z2 & Hz1 & Hz2 & Hz3); auto.
      exists (Oinr z1), (Oinr z2); repeat split; by try constructor.
  - intros n [xl|xr|] [yl|yr|] Hx Hyx; try done.
    + destruct (ora_extend n xl yl) as (z & Hz1 & Hz2); auto.
      exists (Oinl z); repeat split; by try constructor.
    + destruct (ora_extend n xr yr) as (z & Hz1 & Hz2); auto.
      exists (Oinr z); repeat split; by try constructor.
  - intros n [xl|xr|] [yl|yr|] Hyx; inversion Hyx; simplify_eq; try done;
      apply ora_dist_orderN; auto.
  - intros n [xl|xr|] [yl|yr|] Hyx; try done; apply ora_orderN_S; auto.
  - intros n [xl|xr|] [yl|yr|] [zl|zr|] Hxy Hyz; try done;
        eapply ora_orderN_trans; eauto.
  - intros n [xl|xr|] [x'l|x'r|] [yl|yr|] Hxx'; try done; auto;
        eapply ora_orderN_op; eauto.
  - intros n [xl|xr|] [yl|yr|] Hx Hyx; try done;
      eapply ora_validN_orderN; eauto.
  - intros [xl|xr|] [yl|yr|]; split => Hx; try intros n;
    try done; try (by (pose proof (Hx 0) as Hx0; inversion Hx0));
    apply ora_order_orderN; auto.
  - intros [xl|xr|] [cxl|cxr|] [yl|yr|]; simpl in *;
      try destruct (pcore xl) as [cxl'|] eqn:Heq;
      try destruct (pcore xr) as [cxr'|] eqn:Heq;
      simpl in *; inversion 1 as [? ? Heq'|];
        inversion Heq' as [? ? Heq''|? ? Heq''|]; simplify_eq; eauto.
    + destruct (ora_pcore_order_op xl cxl' yl) as [cxy [Hcxy1 Hcxy2]];
        first by rewrite Heq.
      revert Hcxy2; rewrite Heq'' => Hcxy2.
      exists (Oinl cxy); rewrite Hcxy1 /=; auto.
    + destruct (ora_pcore_order_op xr cxr' yr) as [cxy [Hcxy1 Hcxy2]];
        first by rewrite Heq.
      revert Hcxy2; rewrite Heq'' => Hcxy2.
      exists (Oinr cxy); rewrite Hcxy1 /=; auto.
Qed.
Canonical Structure osumR := OraT (osum A B) osum_ora_mixin.

Global Instance osum_ora_discrete :
  OraDiscrete A → OraDiscrete B → OraDiscrete osumR.
Proof.
  split; first apply _.
  by move=>[a|b|] HH /=; try apply ora_discrete_valid.
  move=>[a|b|] [a'|b'|] HH /=; try done.
  - by apply (ora_discrete_order a a').
  - by apply (ora_discrete_order b b').
Qed.

Global Instance Oinl_oracore_id a : OraCoreId a → OraCoreId (Oinl a).
Proof. rewrite /OraCoreId /=. inversion_clear 1; by repeat constructor. Qed.
Global Instance Oinr_oracore_id b : OraCoreId b → OraCoreId (Oinr b).
Proof. rewrite /OraCoreId /=. inversion_clear 1; by repeat constructor. Qed.

Global Instance Oinl_exclusive a : OraExclusive a → OraExclusive (Oinl a).
Proof. by move=> H[]? =>[/H||]. Qed.
Global Instance Oinr_exclusive b : OraExclusive b → OraExclusive (Oinr b).
Proof. by move=> H[]? =>[|/H|]. Qed.

Global Instance Oinl_cancelable a : OraCancelable a → OraCancelable (Oinl a).
Proof.
  move=> ?? [y|y|] [z|z|] ? EQ //; inversion_clear EQ.
  constructor. by eapply (oracancelableN a).
Qed.
Global Instance Oinr_cancelable b : OraCancelable b → OraCancelable (Oinr b).
Proof.
  move=> ?? [y|y|] [z|z|] ? EQ //; inversion_clear EQ.
  constructor. by eapply (oracancelableN b).
Qed.

Global Instance Oinl_id_free a : OraIdFree a → OraIdFree (Oinl a).
Proof. intros ? [] ? EQ; inversion_clear EQ. by eapply oraid_free0_r. Qed.
Global Instance Oinr_id_free b : OraIdFree b → OraIdFree (Oinr b).
Proof. intros ? [] ? EQ; inversion_clear EQ. by eapply oraid_free0_r. Qed.

(** Internalized properties *)
Lemma osum_equivI {M} (x y : osum A B) :
  x ≡ y ⊣⊢ (match x, y with
            | Oinl a, Oinl a' => a ≡ a'
            | Oinr b, Oinr b' => b ≡ b'
            | OsumBot, OsumBot => True
            | _, _ => False
            end : ouPred M).
Proof.
  rewrite /internal_eq /bi_internal_eq_internal_eq /=.
  ouPred.unseal; do 2 split; first by destruct 1.
  by destruct x, y; try destruct 1; try constructor.
Qed.
Lemma osum_validI {M} (x : osum A B) :
  ✓ x ⊣⊢ (match x with
          | Oinl a => ✓ a
          | Oinr b => ✓ b
          | OsumBot => False
          end : ouPred M).
Proof. ouPred.unseal. by destruct x. Qed.

(** Updates *)
(* Lemma osum_update_l (a1 a2 : A) : a1 ~~> a2 → Oinl a1 ~~> Oinl a2. *)
(* Proof. *)
(*   intros Ha n [[a|b|]|] ?; simpl in *; auto. *)
(*   - by apply (Ha n (Some a)). *)
(*   - by apply (Ha n None). *)
(* Qed. *)
(* Lemma osum_update_r (b1 b2 : B) : b1 ~~> b2 → Oinr b1 ~~> Oinr b2. *)
(* Proof. *)
(*   intros Hb n [[a|b|]|] ?; simpl in *; auto. *)
(*   - by apply (Hb n (Some b)). *)
(*   - by apply (Hb n None). *)
(* Qed. *)
(* Lemma osum_updateP_l (P : A → Prop) (Q : osum A B → Prop) a : *)
(*   a ~~>: P → (∀ a', P a' → Q (Oinl a')) → Oinl a ~~>: Q. *)
(* Proof. *)
(*   intros Hx HP n mf Hm. destruct mf as [[a'|b'|]|]; try by destruct Hm. *)
(*   - destruct (Hx n (Some a')) as (c&?&?); naive_solver. *)
(*   - destruct (Hx n None) as (c&?&?); naive_solver eauto using cmra_validN_op_l. *)
(* Qed. *)
(* Lemma osum_updateP_r (P : B → Prop) (Q : osum A B → Prop) b : *)
(*   b ~~>: P → (∀ b', P b' → Q (Oinr b')) → Oinr b  ~~>: Q. *)
(* Proof. *)
(*   intros Hx HP n mf Hm. destruct mf as [[a'|b'|]|]; try by destruct Hm. *)
(*   - destruct (Hx n (Some b')) as (c&?&?); naive_solver. *)
(*   - destruct (Hx n None) as (c&?&?); naive_solver eauto using cmra_validN_op_l. *)
(* Qed. *)
(* Lemma osum_updateP'_l (P : A → Prop) a : *)
(*   a ~~>: P → Oinl a ~~>: λ m', ∃ a', m' = Oinl a' ∧ P a'. *)
(* Proof. eauto using osum_updateP_l. Qed. *)
(* Lemma osum_updateP'_r (P : B → Prop) b : *)
(*   b ~~>: P → Oinr b ~~>: λ m', ∃ b', m' = Oinr b' ∧ P b'. *)
(* Proof. eauto using osum_updateP_r. Qed. *)

(* Lemma osum_local_update_l (a1 a2 a1' a2' : A) : *)
(*   (a1,a2) ~l~> (a1',a2') → (Oinl a1,Oinl a2) ~l~> (Oinl a1',Oinl a2'). *)
(* Proof. *)
(*   intros Hup n mf ? Ha1; simpl in *. *)
(*   destruct (Hup n (mf ≫= maybe Oinl)); auto. *)
(*   { by destruct mf as [[]|]; inversion_clear Ha1. } *)
(*   split. done. by destruct mf as [[]|]; inversion_clear Ha1; constructor. *)
(* Qed. *)
(* Lemma osum_local_update_r (b1 b2 b1' b2' : B) : *)
(*   (b1,b2) ~l~> (b1',b2') → (Oinr b1,Oinr b2) ~l~> (Oinr b1',Oinr b2'). *)
(* Proof. *)
(*   intros Hup n mf ? Ha1; simpl in *. *)
(*   destruct (Hup n (mf ≫= maybe Oinr)); auto. *)
(*   { by destruct mf as [[]|]; inversion_clear Ha1. } *)
(*   split. done. by destruct mf as [[]|]; inversion_clear Ha1; constructor. *)
(* Qed. *)
End ora.

Arguments osumR : clear implicits.

(* Functor *)
Instance osum_map_orara_morphism {A A' B B' : oraT} (f : A → A') (g : B → B') :
  OraMorphism f → OraMorphism g → OraMorphism (osum_map f g).
Proof.
  split; try apply _.
  - intros n [a|b|]; simpl; auto using ora_morphism_validN.
  - intros n [a|b|] [a'|b'|]; simpl; try done;
        rewrite -?Oinl_orderN -?Oinr_orderN; by apply ora_morphism_orderN.
  - intros [a|b|]; rewrite /= -?Increasing_oinl -?Increasing_oinr => ?;
      last apply Increasing_OsumBot; by apply ora_morphism_increasing.
  - move=> [a|b|]=>//=; rewrite ora_morphism_pcore; by destruct pcore.
  - intros [xa|ya|] [xb|yb|]=>//=; by rewrite -ora_morphism_op.
Qed.

Program Definition osumRF (Fa Fb : OrarFunctor) : OrarFunctor := {|
  orarFunctor_car A _ B _ := osumR (orarFunctor_car Fa A B) (orarFunctor_car Fb A B);
  orarFunctor_map A1 _ A2 _ B1 _ B2 _ fg := osumO_map (orarFunctor_map Fa fg) (orarFunctor_map Fb fg)
|}.
Next Obligation.
  by intros Fa Fb A1 ? A2 ? B1 ? B2 ? n f g Hfg; apply osumO_map_ne;
    try apply orarFunctor_map_ne.
Qed.
Next Obligation.
  intros Fa Fb A ? B ? x. rewrite /= -{2}(osum_map_id x).
  apply osum_map_ext=>y; apply orarFunctor_map_id.
Qed.
Next Obligation.
  intros Fa Fb A1 ? A2 ? A3 ? B1 ? B2 ? B3 ? f g f' g' x. rewrite /= -osum_map_compose.
  apply osum_map_ext=>y; apply orarFunctor_map_compose.
Qed.

Instance osumRF_contractive Fa Fb :
  OrarFunctorContractive Fa → OrarFunctorContractive Fb →
  OrarFunctorContractive (osumRF Fa Fb).
Proof.
  by intros ?? A1 ? A2 ? B1 ? B2 ? n f g Hfg; apply osumO_map_ne;
    try apply orarFunctor_map_contractive.
Qed.
